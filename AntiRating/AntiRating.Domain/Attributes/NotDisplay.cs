﻿using System;

namespace AntiRating.Domain.Attributes
{
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class NotDisplayAttribute : Attribute
    {
        public NotDisplay NotDisplayOn { get; set; }

        public NotDisplayAttribute() : this(NotDisplay.OnAll)
        {
        }

        public NotDisplayAttribute(NotDisplay notDisplayOn)
        {
            NotDisplayOn = notDisplayOn;
        }
    }

    public enum NotDisplay
    {
        Undefined = 0,
        OnIndex = 1,
        OnDetails = 2,
        OnCreate = 3,
        OnAll
    }
}