﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using AntiRating.Domain.Entities;

namespace AntiRating.Domain
{
    public class Repository
    {
        public ARDBContext DbContext { set; get; }
        public Repository() : this(new ARDBContext())
        {
        }

        public Repository(ARDBContext context)
        {
            DbContext = context;
        }

        public IQueryable<T> GetAll<T>() where T : class
        {
            return DbContext.Set<T>();
        }
        public IQueryable<T> In<T>() where T : class
        {
            return DbContext.Set<T>();
        }

        public T SingleOrDefault<T>(Func<T, bool> predicate) where T : class
        {
            return DbContext.Set<T>().SingleOrDefault(predicate);
        }


        public void Add<T>(T entity) where T : class
        {
            DbContext.Set<T>().Add(entity);
        }

        public void Update<T>(T entity) where T : class
        {            
            DbContext.Set<T>().Attach(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
        }

        public void Remove<T>(T entity) where T : class
        {
            DbContext.Set<T>().Remove(entity);
        }

        public void SaveChanges()
        {
            try
            {
                DbContext.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            {
                var outputLines = new StringBuilder();
                foreach (var eve in e.EntityValidationErrors)
                {
                    outputLines.AppendLine(string.Format(
                        "{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:",
                        DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.AppendLine(string.Format(
                            "- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage));
                    }
                }

                throw new Exception(outputLines.ToString());
            }
        }


        public void Dispose()
        {
            DbContext.Dispose();
        }

        public IQueryable<T> GetByEntity<T>(Func<T, bool> predicate) where T : class
        {
            return DbContext.Set<T>().Where(predicate).AsQueryable();
        }

        public Profile GetProfileByLogin(string login)
        {
            return DbContext.Profiles.SingleOrDefault(x => x.Login == login);
        }

        //public string[] GetRolesForUser(string username)
        //{
        //    var user = GetProfileByLogin(username);

        //    return new[] { user.Role.ToString() };
        //}
    }
}
