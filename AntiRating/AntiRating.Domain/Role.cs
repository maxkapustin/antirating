﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiRating.Domain
{
    public enum Role
    {
        [Display(Name = "Не выбрано")]
        Undefined = 0,

        [Display(Name = "Пользователь")]
        User =1,

        [Display(Name ="Модератор")]
        Moderator =2,

        [Display(Name = "Администратор")]
        Administrator=3
    }
}
