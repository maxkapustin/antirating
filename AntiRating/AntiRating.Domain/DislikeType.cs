﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiRating.Domain
{
    public enum DislikeType
    {
        [Description("Обвинительный")]
        Accusing = 1,
        [Description("Оправдательный")]
        Justificative =2
    }
}
