﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiRating.Domain.Migration
{
    public class Configuration: DbMigrationsConfiguration<ARDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            // ставим на true, т.к. возможно удаление полей таблицы в которых есть данные, в противном случае не удалит поле
            this.AutomaticMigrationDataLossAllowed = true;
            ContextKey = "Movex.Domain.Concrete.EFDbContext";
        }

        protected override void Seed(ARDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
