﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiRating.Domain
{
    public enum DislikeCategory
    {

        [Description("Мошенник")]
        Fraudster=1,

        [Description("Вор")]
        Thief=2,

        [Description("Взяточник")]
        Bribetaker=3,

        [Description("Брачный аферист")]
        MarriageSwindler=4,

        [Description("Алименты")]
        Alimony=5,

        [Description("Неприязнь")]
        Distaste=6,

        [Description("Обманщики")]
        Deceivers = 7,

        [Description("Измена")]
        Infidelity = 8,

        [Description("Клевета")]
        Slander = 9,

        [Description("Недобросовестные работодатели")]
        UnscrupulousEmployers = 10,

        [Description("Недобросовестные работники")]
        UnscrupulousEmployees = 11


    }
}
