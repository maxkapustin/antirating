﻿using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using AntiRating.Domain.Entities;
using Configuration = AntiRating.Domain.Migration.Configuration;



namespace AntiRating.Domain
{
    public class ARDBContext : DbContext
    {
        public ARDBContext() : base(ConfigurationManager.AppSettings["CatalogDataBase"])
        {
            // при возникновении ошибки
            // The Entity Framework provider type 'System.Data.Entity.SqlServer.SqlProviderServices, EntityFramework.SqlServer' 
            // registered in the application config file for the ADO.NET provider with invariant name 'System.Data.SqlClient' could not be loaded.
            var _ = typeof (System.Data.Entity.SqlServer.SqlProviderServices);

            // установка инициализации бд - стратегии миграции IDatabaseInitializer
            Database.SetInitializer<ARDBContext>(new MigrateDatabaseToLatestVersion<ARDBContext, Configuration>());
        }

        /// <summary>
        /// Профили портала
        /// </summary>
        public DbSet<Profile> Profiles { set; get; }

     
        public DbSet<Claim> Claims { set; get; }

        public DbSet<Dislike> Dislikes { set; get; }

        public DbSet<CountryReference> CountryReferences { set; get; }
        public DbSet<StateReference> StateReferences { set; get; }
        public DbSet<CityReference> CityReferences { set; get; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
