﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiRating.Domain
{
    public enum ClaimObjectType
    {
        [Description("Человек")]
        Person=1,

        [Description("Организация")]
        Organization =2
    }
}
