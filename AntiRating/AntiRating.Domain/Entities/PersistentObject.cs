﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiRating.Domain.Entities
{
    public abstract class PersistentObject
    {

        [Key]
        public int Id { set; get; }


        public bool IsNewObject
        {
            get { return Id == 0; }
        }

    }
}
