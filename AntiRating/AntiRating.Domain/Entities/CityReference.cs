﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AntiRating.Domain.Entities
{
    /// <summary>
    /// Справочник городов
    /// </summary>
    [Table("CityReferences")]    
    public class CityReference
    {

        [Key]
        public Guid Id { set; get; }

        [StringLength(255)]
        public string Info { set; get; }

        [ForeignKey("State")]
        public Guid StateId { set; get; }
        public virtual StateReference State { set; get; }

        public bool BiggestCity { set; get; }

    }
}