﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AntiRating.Domain.Entities
{
    /// <summary>
    /// Справочник регионов
    /// </summary>
    [Table("StateReferences")]    
    public class StateReference 
    {

        [Key]
        public Guid Id { set; get; }

        [StringLength(255)]
        public string Info { set; get; }

        [ForeignKey("Country"), Required]
        public Guid CountryId { set; get; }
        public virtual CountryReference Country { set; get; }
    }
}