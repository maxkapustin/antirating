﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiRating.Domain.Entities
{
    [Table("Docs"), MetadataType(typeof(PersistentObject))]
    public class Doc: PersistentObject
    {
        public byte[] Content { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255), Required]
        public string ContentType { get; set; }
    }
}
