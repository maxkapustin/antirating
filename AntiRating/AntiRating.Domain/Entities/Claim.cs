﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiRating.Domain.Entities
{
    [Table( "Claims")]
    [MetadataType(typeof(PersistentObject))]
    public class Claim : PersistentObject
    {
        [ForeignKey("Creator"), Required]
        public int CreatorId { get; set; }

        public virtual Profile Creator { get; set; }

        public DateTime CreationDate { get; set; }

        public string Name { get; set; }

        public virtual ClaimObjectType ObjectType { get; set; }

        public virtual List<Dislike> Dislikes { get; set; }

        [ForeignKey("Photo")]
        public int? PhotoId { get; set; }
        public virtual Photo Photo { get; set; }

        [ForeignKey("City"), Required]
        public Guid CityId { get; set; }
        public virtual CityReference City { get; set; } 
                
        public int ViewCount { set; get; }   

    }
}
