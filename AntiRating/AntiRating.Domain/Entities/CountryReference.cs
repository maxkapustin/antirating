﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AntiRating.Domain.Entities
{
    /// <summary>
    /// Справочник стран
    /// </summary>
    [Table("CountryReferences")]    
    public class CountryReference 
    {

        [Key]
        public Guid Id { set; get; }

        [StringLength(255)]
        public string Info { set; get; }

        [StringLength(5)]
        public string CurrencyCode { set; get; }

        [StringLength(255)]
        public string Currency { set; get; }
    }
}