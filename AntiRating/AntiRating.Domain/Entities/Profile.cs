﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AntiRating.Domain.Attributes;

namespace AntiRating.Domain.Entities
{

    /// <summary>
    /// Профиль портала.
    /// </summary>
    public class Profile : PersistentObject
    {

        public Profile()
        {
            DateOfRegistr = DateTime.Now;
            //Role = Role.User;
            Locked = false;
            //Users = new List<User>();
        }

        /// <summary>
        /// Признак заблокирован ли пользователь
        /// </summary>

        public bool Locked { get; set; }

       
        public string Login { set; get; }

        public string PersonalAccount { set; get; }

    
        [StringLength(50)]
        public string Email { get; set; }


        [Required]
        [StringLength(50)]
        [DataType(DataType.Password)]
        public string Password { set; get; }

        private Role _role;
        /// <summary>
        /// Роль пользователя
        /// </summary>
        [Required]
        [UIHint("Enum")]
        public Role Role
        {
            get { return _role; }
            set{_role = value;}
        }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        [DataType(DataType.DateTime)]
        public DateTime DateOfRegistr { get; set; }

        [ForeignKey("Photo")]
        public int? PhotoId { get; set; }
        public virtual Photo Photo { get; set; }

        [StringLength(50)]
        public string FirstName { get; set; }

        [StringLength(50)]
        public string SecondName { get; set; }

        public bool EmailVisible { get; set; }

    }
}