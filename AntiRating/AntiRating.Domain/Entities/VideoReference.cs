﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiRating.Domain.Entities
{

    [Table("VideoReference"), MetadataType(typeof(PersistentObject))]
    public class VideoReference:PersistentObject
    {
        public string Reference { get; set; }
    }
}
