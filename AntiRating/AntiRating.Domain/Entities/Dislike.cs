﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AntiRating.Domain.Entities
{
    [Table("Dislikes")]
    [MetadataType(typeof(PersistentObject))]
    public class Dislike: PersistentObject
    {
        public bool MainDislike { set; get; }

        [ForeignKey("CreatorProfile"), Required]
        public int CreatorProfileId { get; set; }
        public virtual Profile CreatorProfile { get; set; }

        public DateTime CreationDate { get; set; }

        [ForeignKey("Claim"), Required]
        public int ClaimId { get; set; }
        public virtual Claim Claim { get; set; }

        public virtual List<Photo> Photos { get; set; }

        public virtual List<Doc> Docs { get; set; }

        public virtual List<VideoReference> VideoReferences { get; set; }

        public string Text { get; set; }

        public DislikeType DislikeType { get; set; }

        public DislikeCategory Category { get; set; }
    }
}
