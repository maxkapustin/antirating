﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AntiRating.Models;
using AntiRating.Domain;
using AntiRating.Domain.Entities;
using AntiRating.Helpers;
using MvcPager;

namespace AntiRating.Controllers
{

    [Authorize]
    public class ClaimController : CommonController
    {        

        public ActionResult Index(int pageIndex = 0)
        {
            var currentProfile = GetCurrentProfile();
            if (currentProfile == null)
                return RedirectToAction("Index", "Home");

            var claims = Repository.GetAll<Claim>().Where(c => c.Creator.Id == currentProfile.Id).OrderByDescending(o=>o.CreationDate).ToPagedList(pageIndex, PageSize).ToList();
            var models = ViewModelHelper.ClaimsToNewVm(claims);

            ViewBag.ProfileInfo = new UserHelper(Repository.DbContext).GetCurrentProfileInfo();

            return View(models.ToPagedList(pageIndex, PageSize));
        }

        [HttpGet]
        public ActionResult Create()
        {
            var profile = GetCurrentProfile();
            if (profile!=null)
                return View(new NewClaimModel { ProfileId = GetCurrentProfile().Id });
            return RedirectToAction("Login", "Account");
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Create(NewClaimModel model)
        {
            if (ModelState.IsValid)
            {                
                var claim= new Claim();
                claim.Name = model.Name;
                claim.ObjectType = model.ObjectType;
                claim.CreationDate = model.CreationDate != null ? model.CreationDate.Value : DateTime.Now;
                claim.CityId = model.CityId.Value;

                if (model.ObjectPhoto.FileBase != null)
                {
                    var objectPhoto = new Photo();
                    objectPhoto.Name = model.ObjectPhoto.FileBase.FileName;
                    objectPhoto.ContentType = model.ObjectPhoto.FileBase.ContentType;
                    objectPhoto.Content = new byte[model.ObjectPhoto.FileBase.ContentLength];
                    model.ObjectPhoto.FileBase.InputStream.Read(objectPhoto.Content, 0, model.ObjectPhoto.FileBase.ContentLength);
                    claim.Photo = objectPhoto;
                }

                var creatorProfile=(new UserHelper(Repository.DbContext)).GetCurrentProfile();
                claim.Creator = creatorProfile;


                var disLike = new Dislike
                {
                    Claim = claim,
                    CreationDate = claim.CreationDate,
                    CreatorProfile = creatorProfile,
                    DislikeType = DislikeType.Accusing,
                    Text = model.Text,
                    MainDislike = true,
                    Category = model.DislikeCategory
                };
                if (model.VideoReferences.Any())
                {
                    disLike.VideoReferences= new List<VideoReference>();
                    foreach (var url in model.VideoReferences)
                    {
                        var videoReference = new VideoReference();
                        videoReference.Reference = url;
                        disLike.VideoReferences.Add(videoReference);

                    }
                }

                if (model.Photos.Any())
                {
                    disLike.Photos= new List<Photo>();
                    foreach (var httpPostedFileBase in model.Photos.Where(f=>f.FileBase != null))
                    {
                        var  photo= new Photo();
                        photo.Name = httpPostedFileBase.FileBase.FileName;
                        photo.ContentType = httpPostedFileBase.FileBase.ContentType;
                        photo.Content= new byte[httpPostedFileBase.FileBase.ContentLength];
                        httpPostedFileBase.FileBase.InputStream.Read(photo.Content, 0, httpPostedFileBase.FileBase.ContentLength);
                        disLike.Photos.Add(photo);
                    }
                }

                if (model.Docs.Any())
                {
                    disLike.Docs= new List<Doc>();
                    foreach (var httpPostedFileBase in model.Docs.Where(f=>f.FileBase!=null))
                    {
                        var doc= new Doc();
                        doc.Name = httpPostedFileBase.FileBase.FileName;
                        doc.ContentType = httpPostedFileBase.FileBase.ContentType;
                        doc.Content = new byte[httpPostedFileBase.FileBase.ContentLength];
                        httpPostedFileBase.FileBase.InputStream.Read(doc.Content, 0, httpPostedFileBase.FileBase.ContentLength);
                        disLike.Docs.Add(doc);
                    }
                }
                Repository.Add(disLike);
                Repository.Add(claim);
                Repository.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var claim = Repository.GetAll<Claim>().FirstOrDefault(c => c.Id == id);

            if (claim == null)
                return RedirectToAction("Index");

            var mainDislike = claim.Dislikes.FirstOrDefault(d => d.MainDislike);

            if (mainDislike == null)
                return RedirectToAction("Index");

            var vm = new NewClaimModel
            {
                Id = claim.Id,
                Name = claim.Name,
                Text = mainDislike.Text,
                MainDislikeId = mainDislike.Id,
                ObjectType = claim.ObjectType,
                DislikeCategory = mainDislike.Category,
                CityId = claim.CityId
            };

            if (claim.Photo != null)
            {
                vm.ObjectPhoto = new FileViewModel
                {
                    Id = claim.Photo.Id,
                    Content = claim.Photo.Content,
                    ContentType = claim.Photo.ContentType,
                    Name = claim.Photo.Name
                };
            }

            if (mainDislike.Photos != null)
            {
                foreach (var photo in mainDislike.Photos.ToList())
                {
                    vm.Photos.Add(new FileViewModel
                    {
                        Id = photo.Id,
                        Content = photo.Content,
                        ContentType = photo.ContentType,
                        Name = photo.Name
                    });
                }
            }


            if (mainDislike.Docs != null)
            {
                foreach (var doc in mainDislike.Docs.ToList())
                {
                    vm.Docs.Add(new FileViewModel
                    {
                        Id = doc.Id,
                        Content = doc.Content,
                        ContentType = doc.ContentType,
                        Name = doc.Name
                    });
                }
            }


            foreach (var videoReference in mainDislike.VideoReferences.ToList())
            {
                vm.VideoReferenceModel.Add(new VideoReferenceModel
                {
                    Id = videoReference.Id,
                    Url = videoReference.Reference
                });
            }

            return View("Edit", vm);
        }

        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Edit(NewClaimModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var existClaim = Repository.GetAll<Claim>().FirstOrDefault(c=>c.Id == model.Id);

            if (existClaim == null)
                return View("Edit", model);

            using (var scope = Repository.DbContext.Database.BeginTransaction())
            {
                try
                {
                    existClaim.Name = model.Name;
                    existClaim.ObjectType = model.ObjectType;
                    existClaim.CreationDate = model.CreationDate != null ? model.CreationDate.Value : DateTime.Now;


                    if (model.ObjectPhoto.FileBase != null)
                    {
                        var objectPhoto = new Photo
                        {
                            Name = model.ObjectPhoto.FileBase.FileName,
                            ContentType = model.ObjectPhoto.FileBase.ContentType,
                            Content = new byte[model.ObjectPhoto.FileBase.ContentLength]
                        };
                        model.ObjectPhoto.FileBase.InputStream.Read(objectPhoto.Content, 0,
                            model.ObjectPhoto.FileBase.ContentLength);
                        
                        existClaim.Photo = objectPhoto;
                    }



                    var creatorProfile = (new UserHelper(Repository.DbContext)).GetCurrentProfile();
                    existClaim.Creator = creatorProfile;


                    var disLike = existClaim.Dislikes.FirstOrDefault(d => d.MainDislike);

                    if (disLike == null)
                    {
                        scope.Rollback();
                        ModelState.AddModelError("", "Ошибка серврера!");
                        return View("Edit", model);
                    }

                    disLike.Text = model.Text;                                            

                    if (model.VideoReferences.Any())
                    {                                                
                        foreach (var url in model.VideoReferences)
                        {
                            var videoReference = new VideoReference();
                            videoReference.Reference = url;
                            disLike.VideoReferences.Add(videoReference);
                        }
                    }

                    if (model.Photos.Any())
                    {                                                
                        foreach (var httpPostedFileBase in model.Photos)
                        {
                            var photo = new Photo
                            {
                                Name = httpPostedFileBase.FileBase.FileName,
                                ContentType = httpPostedFileBase.FileBase.ContentType,
                                Content = new byte[httpPostedFileBase.FileBase.ContentLength]
                            };
                            httpPostedFileBase.FileBase.InputStream.Read(photo.Content, 0,
                                httpPostedFileBase.FileBase.ContentLength);
                            disLike.Photos.Add(photo);
                        }
                    }


                    if (model.Docs.Any())
                    {
                        foreach (var httpPostedFileBase in model.Docs.Where(f => f.FileBase != null))
                        {
                            var doc = new Doc();
                            doc.Name = httpPostedFileBase.FileBase.FileName;
                            doc.ContentType = httpPostedFileBase.FileBase.ContentType;
                            doc.Content = new byte[httpPostedFileBase.FileBase.ContentLength];
                            httpPostedFileBase.FileBase.InputStream.Read(doc.Content, 0, httpPostedFileBase.FileBase.ContentLength);
                            disLike.Docs.Add(doc);
                        }
                    }

                    Repository.Update(disLike);
                    Repository.Update(existClaim);
                    Repository.SaveChanges();

                    scope.Commit();

                    return RedirectToAction("Index");
                }
                catch (Exception)
                {
                    scope.Rollback();
                    throw;
                }
            }
            return View(model);
        }

        public ActionResult GetDocument(int docId)
        {
            var doc = Repository.GetAll<Doc>().FirstOrDefault(d => d.Id == docId);
            return File(doc.Content, doc.ContentType, doc.Name);
        }

        public ActionResult DeleteFileFromClaim(int photoId, int claimId)
        {
            var file = Repository.GetAll<Photo>().FirstOrDefault(f => f.Id == photoId);
            Repository.Remove(file);
            Repository.SaveChanges();

            return RedirectToAction("Edit", "Claim", new {id = claimId});
        }

        public ActionResult DeleteDocumentFromClaim(int docId, int claimId)
        {
            var file = Repository.GetAll<Doc>().FirstOrDefault(f => f.Id == docId);
            Repository.Remove(file);
            Repository.SaveChanges();

            return RedirectToAction("Edit", "Claim", new { id = claimId });
        }
        public ActionResult DeleteVideoFromClaim(int videoId, int claimId)
        {
            var video = Repository.GetAll<VideoReference>().FirstOrDefault(f => f.Id == videoId);
            Repository.Remove(video);
            Repository.SaveChanges();

            return RedirectToAction("Edit", "Claim", new {id = claimId});
        }

        [HttpGet]
        public ActionResult Read(int id)
        {
            var claimModel= new ClaimModel();
            var claimEntity = Repository.SingleOrDefault<Claim>(c => c.Id == id);
            if (claimEntity == null)
                return RedirectToAction("Index");

            claimModel.Id = claimEntity.Id;
            claimModel.CreationDate = claimEntity.CreationDate;
            claimModel.Name = claimEntity.Name;
            claimModel.ObjectType = claimEntity.ObjectType;
            claimModel.ProfileId =claimEntity.CreatorId;
            if (claimEntity.Photo != null)
            {
                var photoVm = new FileViewModel
                {
                    Content = claimEntity.Photo.Content,
                    ContentType = claimEntity.Photo.ContentType,
                    Name = claimEntity.Photo.Name
                };
                claimModel.ObjectPhoto = photoVm;
            }

            claimModel.Dislikes = new List<DislikeModel>();
            foreach (var dislike in claimEntity.Dislikes)
            {
                var dislikeModel = new DislikeModel();
                dislikeModel.Text = dislike.Text;
                dislikeModel.DislikeType = dislike.DislikeType;
                
                for (var i = 0; i < dislike.VideoReferences.Count; i++)
                {
                    dislikeModel.VideoReferences[i] = dislike.VideoReferences[i].Reference;
                }
                dislikeModel.Photos= new List<FileViewModel>();
                dislikeModel.Category = dislike.Category;
                var author= new UserVM();
                author.Id = dislike.CreatorProfile.Id;
                author.Login = dislike.CreatorProfile.Login;
                dislikeModel.Author = author;

                
                dislikeModel.CreationDate = dislike.CreationDate;
                claimModel.Dislikes.Add(dislikeModel);
            }

            return View("Read",claimModel);
        }
    }
}