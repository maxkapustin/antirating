﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AntiRating.Domain.Entities;
using AntiRating.Models;

namespace AntiRating.Controllers
{

    [Authorize]
    public class UserDataController : CommonController
    {
        public ActionResult Index()
        {            
            return View(new UserHelper(Repository.DbContext).GetCurrentProfileInfo());
        }

        [HttpPost]
        public ActionResult Save(ProfileModel profileModel)
        {
            if (ModelState.IsValid)
            {
                var profileEntity = GetCurrentProfile();
                if (profileEntity != null)
                {
                    profileEntity.Email = profileModel.Email;
                    profileEntity.EmailVisible = profileModel.EmailVisible;
                    profileEntity.FirstName = profileModel.FirstName;
                    profileEntity.Login = profileModel.Login;
                    profileEntity.SecondName = profileModel.SecondName;

                    if (profileModel.Photo.FileBase != null)
                    {
                        var objectPhoto = new Photo
                        {
                            Name = profileModel.Photo.FileBase.FileName,
                            ContentType = profileModel.Photo.FileBase.ContentType,
                            Content = new byte[profileModel.Photo.FileBase.ContentLength]
                        };
                        profileModel.Photo.FileBase.InputStream.Read(objectPhoto.Content, 0,
                            profileModel.Photo.FileBase.ContentLength);

                        profileEntity.Photo = objectPhoto;
                    }

                    Repository.Update(profileEntity);
                    Repository.SaveChanges();
                    return RedirectToAction("Index", "Claim");

                }

            }
            return RedirectToAction("Index");
        }


    }
}