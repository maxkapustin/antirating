﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AntiRating.Domain.Entities;
using AntiRating.Helpers;
using AntiRating.Models.CountriesApi;

namespace AntiRating.Controllers
{
    public class ReferenceController : CommonController
    {        
        public ActionResult GetCountries()
        {
            var res = Repository.GetAll<CountryReference>().Select(s=> new { s.Id, s.Info }).ToList();
            return Json(new {data = res, success = true }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GetStates(Guid countryId)
        {
            var res = Repository.GetAll<StateReference>().Where(s=>s.CountryId == countryId).Select(s => new { s.Id, s.Info }).ToList();
            return Json(new { data = res, success = true }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCities(Guid stateId)
        {
            var res = Repository.GetAll<CityReference>().Where(s => s.StateId == stateId).Select(s => new { s.Id, s.Info }).ToList();
            return Json(new { data = res, success = true }, JsonRequestBehavior.AllowGet);
        }
    }
}