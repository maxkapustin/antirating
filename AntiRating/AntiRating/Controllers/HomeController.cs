﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AntiRating.Domain;
using AntiRating.Domain.Entities;
using AntiRating.Helpers;
using AntiRating.Models;
using MvcPager;

namespace AntiRating.Controllers
{
    [Authorize]
    public class HomeController : CommonController
    {
        private int topCount = 100;

        public ActionResult Serarch(SearchModel model)
        {
            var queryable = Repository.GetAll<Claim>();
            if (!string.IsNullOrEmpty(model.FilterName))
            {
                queryable = queryable.Where(q => q.Name.Contains(model.FilterName));
            }

            if (model.FilterCity.HasValue)
            {
                queryable = queryable.Where(q => q.CityId == model.FilterCity);
            }
            else if (model.FilterState.HasValue)
            {
                queryable = queryable.Where(q => q.City.StateId == model.FilterState);
            }
            else if (model.FilterCountry.HasValue)
            {
                queryable = queryable.Where(q => q.City.State.CountryId == model.FilterCountry);
            }

            var claims =
                queryable                    
                    .OrderByDescending(o => o.CreationDate)
                    .Take(50)
                    .ToList();

            return View("SearchResult", ViewModelHelper.ClaimsToNewVm(claims));
        }

        public ActionResult Index()
        {
            var dislikesList = GetMyLast4Dislikes();

            if (!dislikesList.Any())
            {
                dislikesList = GetLast4Dislikes();
                ViewBag.LastAny4Disliked = true;
            }

            var mainRatings = new MainRating
            {                                
                FraudsterRatings = GetTopList(DislikeCategory.Fraudster),                
                ThiefRatings = GetTopList(DislikeCategory.Thief),
                BribetakerRatings = GetTopList(DislikeCategory.Bribetaker),
                MarriageSwindlerRatings = GetTopList(DislikeCategory.MarriageSwindler),
                AlimonyRatings = GetTopList(DislikeCategory.Alimony),
                DistasteRatings = GetTopList(DislikeCategory.Distaste),
                DeceiversRatings = GetTopList(DislikeCategory.Deceivers),
                InfidelityRatings = GetTopList(DislikeCategory.Infidelity),
                SlanderRatings = GetTopList(DislikeCategory.Slander),
                UnscrupulousEmployersRatings = GetTopList(DislikeCategory.UnscrupulousEmployers),
                UnscrupulousEmployeesRatings = GetTopList(DislikeCategory.UnscrupulousEmployees),
                MyTop4Dislikes = dislikesList
            };


            return View(mainRatings);
        }

        public ActionResult GetPhotosOfDislike(int dislikeId)
        {
            var dislike = Repository.GetAll<Dislike>().FirstOrDefault(d => d.Id == dislikeId);
            if (dislike == null)
                return null;

            var photosId = dislike.Photos.Select(p=>p.Id).ToList();
            return PartialView("_dislikePhotoCarusel", photosId);
        }

        public ActionResult GetVidoesOfDislike(int dislikeId)
        {
            var dislike = Repository.GetAll<Dislike>().FirstOrDefault(d => d.Id == dislikeId);
            if (dislike == null)
                return null;

            var videoIds = dislike.VideoReferences.Select(p=>p.Reference).ToList();
            return PartialView("_dislikeVideoList", videoIds);
        }

        public ActionResult GetDocsOfDislike(int dislikeId)
        {
            var dislike = Repository.GetAll<Dislike>().FirstOrDefault(d => d.Id == dislikeId);
            if (dislike == null)
                return null;
            var result= new List<Tuple<int,string>>();

            foreach (var doc in dislike.Docs.ToList())
            {
                result.Add(new Tuple<int, string>(doc.Id,doc.Name));
            }
            ;
            return PartialView("_dislikeDocList", result);
        }

        public ActionResult GetDocument(int docId)
        {
            var doc = Repository.GetAll<Doc>().FirstOrDefault(d => d.Id == docId);
            return File(doc.Content, doc.ContentType, doc.Name);
        }

        private List<DislikeModel> GetLast4Dislikes()
        {            
            var dislikes = Repository.GetAll<Dislike>().OrderByDescending(o=>o.CreationDate).Take(4).ToList();

             var res = new List<DislikeModel>();
            foreach (var dislike in dislikes)
            {
                var dislikeToVm = ViewModelHelper.DislikeToVm(dislike);
                dislikeToVm.DislikeCounts = dislike.Claim.Dislikes.Count();
                dislikeToVm.VideoRefCount = dislike.VideoReferences.Count();
                dislikeToVm.PhotosCount = dislike.Photos.Count();
                dislikeToVm.DocsCount = dislike.Docs.Count();
                res.Add(dislikeToVm);
            } 

            return res;
        }
        private List<DislikeModel> GetMyLast4Dislikes()
        {
            var profile = GetCurrentProfile();
            var dislikes = Repository.GetAll<Dislike>().Where(d => d.CreatorProfileId == profile.Id).OrderByDescending(o=>o.CreationDate).Take(4).ToList();

            var res = new List<DislikeModel>();
            foreach (var dislike in dislikes)
            {
                var dislikeToVm = ViewModelHelper.DislikeToVm(dislike);
                dislikeToVm.DislikeCounts = dislike.Claim.Dislikes.Count();
                dislikeToVm.VideoRefCount = dislike.VideoReferences.Count();
                dislikeToVm.PhotosCount = dislike.Photos.Count();
                dislikeToVm.DocsCount = dislike.Docs.Count();
                res.Add(dislikeToVm);
            }

            return res;
        }

        public List<ClaimModel> GetTopList(DislikeCategory category)
        {
            var claims =
                Repository.GetAll<Claim>()
                    .Where(x => x.Dislikes.Any(d => d.Category == category))
                    .OrderByDescending(c => c.Dislikes.Count(y => y.Category == category)).Take(topCount)
                    .ToList();
            var claimVMs= ViewModelHelper.ClaimsToVm(claims);
            foreach (var claimModel in claimVMs)
            {
                claimModel.CurrentCategoryDislikeCount =
                    claims
                        .First(x => x.Id == claimModel.Id)
                        .Dislikes.Count(d => d.Category == category);
            }
            return claimVMs;
        }

        private List<NewClaimModel> GetTopByCategory(int pageIndex, DislikeCategory dislikeCategory)
        {
            var claims = Repository.
                GetAll<Claim>().
                Select(
                    c => new
                    {
                        Claim = c,
                        CountDislikes = c.Dislikes.Count(d => d.Category == dislikeCategory)
                    }).
                Where(d => d.CountDislikes > 0).
                OrderByDescending(o => o.CountDislikes).
                Select(s => s.Claim).
                ToPagedList(pageIndex, PageSize).ToList();

            var claimsToNewVm = ViewModelHelper.ClaimsToNewVm(claims);
            return claimsToNewVm;
        }

        /// <summary>
        /// Топ мошенники
        /// </summary>        
        public ActionResult TopFraudster(int pageIndex = 0)
        {            
            return View(GetTopByCategory(pageIndex, DislikeCategory.Fraudster).ToPagedList(pageIndex, PageSize));
        }        

        /// <summary>
        /// Топ воры
        /// </summary>        
        public ActionResult TopThief(int pageIndex = 0)
        {    
            return View(GetTopByCategory(pageIndex, DislikeCategory.Thief).ToPagedList(pageIndex, PageSize));
        }

        /// <summary>
        /// Топ взяточники
        /// </summary>        
        public ActionResult TopBribetaker(int pageIndex = 0)
        {                        
            return View(GetTopByCategory(pageIndex, DislikeCategory.Bribetaker).ToPagedList(pageIndex, PageSize));
        }
        
        /// <summary>
        /// Топ взяточники
        /// </summary>        
        public ActionResult TopMarriageSwindler(int pageIndex = 0)
        {            
            return View(GetTopByCategory(pageIndex, DislikeCategory.MarriageSwindler).ToPagedList(pageIndex, PageSize));
        }

        /// <summary>
        /// Топ алименты
        /// </summary>        
        public ActionResult TopAlimony(int pageIndex = 0)
        {            
            return View(GetTopByCategory(pageIndex, DislikeCategory.Alimony).ToPagedList(pageIndex, PageSize));
        }
        
        /// <summary>
        /// Топ неприязнь
        /// </summary>        
        public ActionResult TopDistaste(int pageIndex = 0)
        {            
            return View(GetTopByCategory(pageIndex, DislikeCategory.Distaste).ToPagedList(pageIndex, PageSize));
        }

        /// <summary>
        /// Топ обманщики
        /// </summary>        
        public ActionResult TopDeceivers(int pageIndex = 0)
        {            
            return View(GetTopByCategory(pageIndex, DislikeCategory.Deceivers).ToPagedList(pageIndex, PageSize));
        }

        /// <summary>
        /// Топ измена
        /// </summary>        
        public ActionResult TopInfidelity(int pageIndex = 0)
        {            
            return View(GetTopByCategory(pageIndex, DislikeCategory.Infidelity).ToPagedList(pageIndex, PageSize));
        }

        /// <summary>
        /// Топ клевета
        /// </summary>        
        public ActionResult TopSlander(int pageIndex = 0)
        {            
            return View(GetTopByCategory(pageIndex, DislikeCategory.Slander).ToPagedList(pageIndex, PageSize));
        }

        /// <summary>
        /// Топ Недобросовестные работодатели
        /// </summary>        
        public ActionResult TopUnscrupulousEmployers(int pageIndex = 0)
        {            
            return View(GetTopByCategory(pageIndex, DislikeCategory.UnscrupulousEmployers).ToPagedList(pageIndex, PageSize));
        }

        /// <summary>
        /// Топ Недобросовестные работники
        /// </summary>        
        public ActionResult TopUnscrupulousEmployees(int pageIndex = 0)
        {            
            return View(GetTopByCategory(pageIndex, DislikeCategory.UnscrupulousEmployees).ToPagedList(pageIndex, PageSize));
        }

    }
}