﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Web;
using System.Web.Mvc;
using AntiRating.Attributes;
using AntiRating.Domain;
using AntiRating.Domain.Entities;

namespace AntiRating.Controllers
{
    [HandleException]
    public class CommonController : Controller
    {
        //protected override void OnActionExecuted(ActionExecutedContext filterContext)
        //{
        //    Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
        //    base.OnActionExecuted(filterContext);
        //}

        protected readonly Repository Repository = new Repository();

        protected const int PageSize = 15;

        protected Profile GetCurrentProfile()
        {
            var profile = Repository.GetProfileByLogin(User.Identity.Name);
            return profile;
        }

      
        public void Attention(string message)
        {
            TempData.Add(Alerts.ATTENTION, message);
        }

        public void Success(string message)
        {
            TempData.Add(Alerts.SUCCESS, message);
        }

        public void Information(string message)
        {
            TempData.Add(Alerts.INFORMATION, message);
        }

        public void Error(string message)
        {
            TempData.Add(Alerts.ERROR, message);
        }

        protected override void Dispose(bool disposing)
        {
            Repository.Dispose();

            base.Dispose(disposing);
        }

        /// <summary>
        /// Доступ к объекту имущества
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns></returns>
        protected bool CheckUserOwner(int profileId)
        {
            var currentUser = GetCurrentProfile();
            if (profileId == currentUser.Id)
            {
                return true;
            }
            return false;
        }

        public ActionResult AccessOwnerDenied()
        {
            return View("AccessOwnerDenied");
        }

        public void ShowModalMessage(string message)
        {
            ViewBag.MessageInvalidaActionOrException = message;
        }

        protected ActionResult RedirectToLogin()
        {
            return RedirectToAction("Login", "Account", new { returnUrl = HttpContext.Request.RawUrl });
        }

        protected void ValidateAccessUser(int? id)
        {
            if (id.HasValue)
                ValidateAccessUser(id.Value);
        }
        protected void ValidateAccessUser(int id)
        {
            var profile = ValidateAccessUser();
        }
        protected Profile ValidateAccessUser()
        {
            var profile = new UserHelper(Repository.DbContext).GetCurrentProfile();
            if (profile == null)
                throw new AuthenticationException("Необходимо авторизоваться");
            return profile;
        }
    }

  
}