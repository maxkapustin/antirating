﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AntiRating.Domain;
using AntiRating.Domain.Entities;
using AntiRating.Models;

namespace AntiRating.Controllers
{
    public class AccountController : CommonController
    {

        [AllowAnonymous]
        public ActionResult Register(string returnUrl)
        {
            var model = new RegisterProfileModel { ReturnUrl = returnUrl };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterProfileModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var searchProfile = Repository.GetProfileByLogin(model.Login);
                    if (searchProfile != null)
                    {
                        ModelState.AddModelError("", string.Format("Пользователь с логином {0}, уже зарегистрирован в системе.", model.Login));
                        return View(model);
                    }
                                   
                    var profile = new Profile();                             

                    profile.Login = model.Login;
                    profile.Password = model.Password;
                    profile.Role = Role.User;
                    profile.Email = model.Email;                                
                    profile.PersonalAccount = (profile.Id + 555).ToString().PadLeft(6, '0');

                    Repository.Add(profile);
                    Repository.SaveChanges();

                    return RedirectToAction("Login");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", e.Message);
                }
            }
            return View(model);
        }


        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var user = Repository.GetProfileByLogin(model.Login);
                if (user != null && user.Password == model.Password)
                {
                    FormsAuthentication.SetAuthCookie(model.Login, model.RememberMe);
                    if (returnUrl == "/" || string.IsNullOrEmpty(returnUrl))
                    {
                        return RedirectToAction("Index", "Home");
                    }
                    return RedirectToLocal(returnUrl);
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "неправильный аккаунт");
            return View(model);
        }

        [HttpGet]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult ChangePassword(string returnUrl)
        {
            var model = new ChangePasswordProfileModel();

            return View(model);
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordProfileModel model)
        {
            if (ModelState.IsValid)
            {
                

                var profile = GetCurrentProfile();
                if (profile != null)
                {
                    if (profile.Password != model.OldPassword)
                    {
                        ModelState.AddModelError("","Неверный пароль!");
                        return View(model);
                    }
                    profile.Password = model.Password;
                    Repository.Update(profile);
                    Repository.SaveChanges();
                    return RedirectToAction("Index", "UserData");
                }
            }
            return View(model);
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (!string.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                return Redirect(returnUrl);

            return RedirectToAction("Index", "Home");
        }
    }
}