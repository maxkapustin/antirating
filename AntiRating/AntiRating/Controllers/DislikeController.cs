﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AntiRating.Domain;
using AntiRating.Domain.Entities;
using AntiRating.Helpers;
using AntiRating.Models;

namespace AntiRating.Controllers
{

    [Authorize]
    public class DislikeController : CommonController
    {
        [HttpGet]
        public ActionResult Add(int claimId)
        {

            var claimEntity = Repository.GetAll<Claim>().FirstOrDefault(x => x.Id == claimId);
            if (claimEntity == null)
                return RedirectToAction("Index", "Home");

            CalculateViewCount(claimEntity);

            var creatorProfile = (new UserHelper(Repository.DbContext)).GetCurrentProfile();
            if (creatorProfile == null)
                return RedirectToAction("Login", "Account");

            var model = new NewDislikeModel();

            var claim = new NewClaimModel
            {
                Id = claimEntity.Id,
                CreationDate = claimEntity.CreationDate,
                Name = claimEntity.Name,
                ObjectType = claimEntity.ObjectType,
                ProfileId = claimEntity.CreatorId ,
                ViewCount = claimEntity.ViewCount
            };
            if (claimEntity.Photo != null)
            {
                var photoVm = new FileViewModel
                {
                    Content = claimEntity.Photo.Content,
                    ContentType = claimEntity.Photo.ContentType,
                    Name = claimEntity.Photo.Name
                };
                claim.ObjectPhoto = photoVm;
            }
            foreach (var dislike in claimEntity.Dislikes)
            {
                claim.Dislikes.Add(ViewModelHelper.DislikeToVm(dislike));
            }

            model.Claim = claim;            
            return View(model);
        }

        private void CalculateViewCount(Claim claim)
        {
            const string uniqueCookiesKey = "7e8d0cea-c7b6-48c8-a920-a9caa67d63b1";
            try
            {
                
                if (Request.Cookies["uniqueCookiesKey"] != null)
                {
                    return;
                }
                var myCookie = new HttpCookie(uniqueCookiesKey, "True") {Expires = DateTime.Now.AddHours(1)};
                Response.Cookies.Add(myCookie);

                claim.ViewCount = claim.ViewCount + 1;
                Repository.Update(claim);
                Repository.SaveChanges();
            }
            catch (Exception)
            {                
            }
        }


        [HttpPost, ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult Add(NewDislikeModel model)
        {
            if (ModelState.IsValid)
            {
                var dislikeEntity = new Dislike();
                dislikeEntity.CreatorProfile = (new UserHelper(Repository.DbContext)).GetCurrentProfile();
                dislikeEntity.Text = model.Text;

                var claimEntity = Repository.GetAll<Claim>().FirstOrDefault(x => x.Id == model.ClaimId);
                dislikeEntity.Claim = claimEntity;

                dislikeEntity.CreationDate = model.CreationDate != null ? model.CreationDate.Value : DateTime.Now;
                dislikeEntity.Photos = new List<Photo>();
                if (model.Photos != null && model.Photos.Length > 0)
                {

                    foreach (var httpPostedFileBase in model.Photos)
                    {
                        if (httpPostedFileBase != null)
                        {
                            var photo = new Photo();
                            photo.Name = httpPostedFileBase.FileName;
                            photo.ContentType = httpPostedFileBase.ContentType;
                            photo.Content = new byte[httpPostedFileBase.ContentLength];
                            httpPostedFileBase.InputStream.Read(photo.Content, 0, httpPostedFileBase.ContentLength);
                            dislikeEntity.Photos.Add(photo);
                        }

                    }
                }

                dislikeEntity.VideoReferences = new List<VideoReference>();
                if (model.VideoReferences != null && model.VideoReferences.Length > 0)
                {
                    foreach (var url in model.VideoReferences)
                    {
                        var videoReference = new VideoReference();
                        videoReference.Reference = url;
                        dislikeEntity.VideoReferences.Add(videoReference);

                    }
                }
                dislikeEntity.Docs = new List<Doc>();
                if (model.Docs != null && model.Docs.Any())
                {
                    foreach (var httpPostedFileBase in model.Docs)
                    {
                        if (httpPostedFileBase != null)
                        {
                            var doc = new Doc();
                            doc.Name = httpPostedFileBase.FileName;
                            doc.ContentType = httpPostedFileBase.ContentType;
                            doc.Content = new byte[httpPostedFileBase.ContentLength];
                            httpPostedFileBase.InputStream.Read(doc.Content, 0, httpPostedFileBase.ContentLength);
                            dislikeEntity.Docs.Add(doc);
                        }
                        
                    }
                }
                dislikeEntity.Category = model.Category;
                dislikeEntity.DislikeType = DislikeType.Accusing;
                Repository.Add(dislikeEntity);
                Repository.SaveChanges();

            }
            return RedirectToAction("Add", new {claimId = model.ClaimId});
        }


        [HttpGet]
        public ActionResult Read(int id)
        {
            var dislikeEntity = Repository.SingleOrDefault<Dislike>(d => d.Id == id);
            if (dislikeEntity != null)
            {
                var dislikeModel = new DislikeModel();
                dislikeModel.Text = dislikeEntity.Text;
                dislikeModel.DislikeType = dislikeEntity.DislikeType;
                dislikeModel.Category = dislikeEntity.Category;
                dislikeModel.Id = dislikeEntity.Id;

                dislikeEntity.VideoReferences = new List<VideoReference>();
                foreach (VideoReference reference in dislikeEntity.VideoReferences)
                {
                    dislikeModel.VideoReferences.Add(reference.Reference);
                }

                var author = new UserVM();
                author.Id = dislikeEntity.CreatorProfile.Id;
                author.Login = dislikeEntity.CreatorProfile.Login;
                dislikeModel.Author = author;

                dislikeModel.Photos = new List<FileViewModel>();
                if (dislikeEntity.Photos != null)
                {
                    foreach (var photo in dislikeEntity.Photos)
                    {
                        var photoModel = new FileViewModel()
                        {
                            Id = photo.Id,
                            Content = photo.Content,
                            ContentType = photo.ContentType,
                            Name = photo.Name
                        };
                        dislikeModel.Photos.Add(photoModel);
                    }
                }
                dislikeModel.CreationDate = dislikeEntity.CreationDate;
                return View("Read", dislikeModel);
            }



            return View("Error");
        }


        public ActionResult DeleteVideoReferenceFromDislike(string refText, int dislikeId)
        {
            var file = Repository.GetAll<VideoReference>().FirstOrDefault(f => f.Reference == refText);
            Repository.Remove(file);
            Repository.SaveChanges();

            return RedirectToAction("Edit", "Dislike", new { id = dislikeId });
        }

        public ActionResult DeleteFileFromDislike(int photoId, int dislikeId)
        {
            var file = Repository.GetAll<Photo>().FirstOrDefault(f => f.Id == photoId);
            Repository.Remove(file);
            Repository.SaveChanges();

            return RedirectToAction("Edit", "Dislike", new { id = dislikeId });
        }


        [HttpGet]
        public ActionResult Edit(int id)
        {
            var dislikeEntity = Repository.SingleOrDefault<Dislike>(d => d.Id == id);
            if (dislikeEntity != null)
            {
                var dislikeModel = new DislikeModel();
                dislikeModel.Text = dislikeEntity.Text;
                dislikeModel.DislikeType = DislikeType.Accusing;
                dislikeModel.Category = dislikeEntity.Category;
                dislikeModel.Id = dislikeEntity.Id;
                dislikeEntity.VideoReferences = new List<VideoReference>();
                foreach (VideoReference reference in dislikeEntity.VideoReferences)
                {
                    dislikeModel.VideoReferences.Add(reference.Reference);
                }

                var author = new UserVM();
                author.Id = dislikeEntity.CreatorProfile.Id;
                author.Login = dislikeEntity.CreatorProfile.Login;
                dislikeModel.Author = author;
                dislikeModel.Photos = new List<FileViewModel>();
                if (dislikeEntity.Photos != null)
                {
                    foreach (var photo in dislikeEntity.Photos)
                    {
                        var photoModel = new FileViewModel()
                        {
                            Id = photo.Id,
                            Content = photo.Content,
                            ContentType = photo.ContentType,
                            Name = photo.Name
                        };
                        dislikeModel.Photos.Add(photoModel);
                    }
                }
                dislikeModel.CreationDate = dislikeEntity.CreationDate;
                return View("Edit", dislikeModel);
            }

            return RedirectToAction("Index", "Home");


        }


        [HttpPost, ValidateAntiForgeryToken, ValidateInput(false)]
        public ActionResult Edit(DislikeModel dislikeModel)
        {
            if (!ModelState.IsValid)
                return View(dislikeModel);

            var existingDislikeEntity = Repository.GetAll<Dislike>().FirstOrDefault(x => x.Id == dislikeModel.Id);
            if (existingDislikeEntity == null)
            {
                return View("Edit", dislikeModel);
            }
            using (var scope = Repository.DbContext.Database.BeginTransaction())
            {
                try
                {
                    var creatorProfile = (new UserHelper(Repository.DbContext)).GetCurrentProfile();
                    existingDislikeEntity.Text = dislikeModel.Text;
                    existingDislikeEntity.Category = dislikeModel.Category;
                    existingDislikeEntity.DislikeType =DislikeType.Accusing;
                    if (dislikeModel.VideoReferences.Any())
                    {
                        var en = existingDislikeEntity.VideoReferences.ToList().GetEnumerator();
                        while (en.MoveNext())
                        {
                            Repository.Remove(en.Current);
                        }
                        Repository.SaveChanges();

                        foreach (var url in dislikeModel.VideoReferences)
                        {
                            var videoReference = new VideoReference();
                            videoReference.Reference = url;
                            existingDislikeEntity.VideoReferences.Add(videoReference);
                        }
                    }

                    if (dislikeModel.Photos.Any())
                    {
                        foreach (var httpPostedFileBase in dislikeModel.Photos)
                        {
                            var photo = new Photo
                            {
                                Name = httpPostedFileBase.FileBase.FileName,
                                ContentType = httpPostedFileBase.FileBase.ContentType,
                                Content = new byte[httpPostedFileBase.FileBase.ContentLength]
                            };
                            httpPostedFileBase.FileBase.InputStream.Read(photo.Content, 0,
                                httpPostedFileBase.FileBase.ContentLength);
                            existingDislikeEntity.Photos.Add(photo);
                        }
                    }

                    Repository.Update(existingDislikeEntity);
                    Repository.SaveChanges();

                    scope.Commit();

                    return RedirectToAction("Index", "Home");
                }
                catch (Exception)
                {
                    scope.Rollback();
                    throw;
                }
            }
        }

        public ActionResult ViewAll()
        {
            var model= new List<DislikeModel>();
            var currentProfile = (new UserHelper(Repository.DbContext)).GetCurrentProfile();
            var dislikeEntities = Repository.GetAll<Dislike>().Where(d => d.CreatorProfileId == currentProfile.Id).ToList();
            foreach (var dislikeEntity in dislikeEntities)
            {
                var dislikeModel = ViewModelHelper.DislikeToVm(dislikeEntity);
                model.Add(dislikeModel);
            }

            return View(model);
        }
    }
}