﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AntiRating.Domain.Entities;

namespace AntiRating.Controllers
{

    [Authorize]
    public class FileController : CommonController
    {
        // GET: File
        public ActionResult GetPhoto(int id)
        {
            var file = Repository.GetAll<Photo>().First(f => f.Id == id);
            return File(file.Content, file.ContentType, file.Name);
        }
    }
}