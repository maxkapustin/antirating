﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AntiRating.Domain;
using AntiRating.Domain.Entities;
using AntiRating.Models;

namespace AntiRating
{
    public class UserHelper
    {
        private readonly ARDBContext _context;

        public UserHelper(ARDBContext context)
        {
            _context = context;
        }

        private const string DraftIdentifireKey = "DRAFT_IDENTIFIRE_KEY_COOKIES";

        public static Profile GetProfileByLogin(string login)
        {
            var repository = new Repository();
            var profile = repository.GetAll<Profile>().FirstOrDefault(p => p.Login == login);
            return profile;
        }

       
        public Profile GetCurrentProfile()
        {

            if (HttpContext.Current.User != null && HttpContext.Current.User.Identity != null
                && !string.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
                return _context.Profiles
                    .FirstOrDefault(p => p.Login == HttpContext.Current.User.Identity.Name);
            return null;
        }


        public ProfileModel GetCurrentProfileInfo()
        {
            var profile = GetCurrentProfile();

            var profileModel = new ProfileModel()
            {
                Login = profile.Login,
                FirstName = profile.FirstName,
                SecondName = profile.SecondName,
                Email = profile.Email,
                EmailVisible = profile.EmailVisible
            };
            if (profile.Photo != null)
            {
                profileModel.Photo = new FileViewModel()
                {
                    Id = profile.Photo.Id,
                    Content = profile.Photo.Content,
                    ContentType = profile.Photo.ContentType,
                    Name = profile.Photo.Name
                };
            }
            if (new Repository()
                .GetAll<Claim>().Any(x => x.CreatorId == profile.Id))
            {
                profileModel.BonusDislikeCount =
                    new Repository().GetAll<Claim>()
                        .Where(x => x.CreatorId == profile.Id)
                            .Select(c => c.Dislikes.Count).Sum();
            }
         

            
            return profileModel;
        }
    }
}