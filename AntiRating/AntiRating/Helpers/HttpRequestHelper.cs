﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace AntiRating.Helpers
{
    public static class HttpRequestHelper
    {

        public static string SendGetRequest(string url, params Cookie[] cookies)
        {
            var resp = SendRequest(url, cookies);

            if (resp.StatusCode == HttpStatusCode.OK)
            {
                var str = new StreamReader(resp.GetResponseStream()).ReadToEnd();
                return str;
            }

            return null;
        }

        private static HttpWebResponse SendRequest(string url, Cookie[] cookies)
        {
            HttpWebRequest rq = (HttpWebRequest) WebRequest.Create(url);

            if (cookies != null && cookies.Any())
            {
                rq.CookieContainer = new CookieContainer();
                foreach (var cookie in cookies)
                {
                    rq.CookieContainer.Add(cookie);
                }
            }

            HttpWebResponse resp = (HttpWebResponse) rq.GetResponse();
            return resp;
        }

        public static T SendGetRequest<T>(string url, params Cookie[] cookies)
        {
            var resp = SendRequest(url, cookies);            

            if (resp.StatusCode == HttpStatusCode.OK)
            {
                var str = new StreamReader(resp.GetResponseStream()).ReadToEnd();
                return new JavaScriptSerializer().Deserialize<T>(str);
            }

            return default(T);
        }
    }

    
}