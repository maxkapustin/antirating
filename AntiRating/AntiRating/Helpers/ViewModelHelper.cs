﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AntiRating.Domain.Entities;
using AntiRating.Models;
using MvcPager;

namespace AntiRating.Helpers
{
    public static class ViewModelHelper
    {
        public static List<NewClaimModel> ClaimsToNewVm(List<Claim> claims)
        {
            var res = new List<NewClaimModel>();
            foreach (var claim in claims)
            {

                var mainDislike = claim.Dislikes.FirstOrDefault(d => d.MainDislike);

                if (mainDislike == null)
                    continue;

                var vm = new NewClaimModel
                {
                    Id = claim.Id,
                    Name = claim.Name,
                    Text = mainDislike.Text,
                    MainDislikeId = mainDislike.Id,
                    ObjectType = claim.ObjectType,
                    DislikeCategory = mainDislike.Category,
                    CreationDate = claim.CreationDate,
                    Region = string.Format("{0} {1}", claim.City.State.Country.Info, claim.City.Info),
                    ViewCount = claim.ViewCount
                };

                if (claim.Photo != null)
                {
                    vm.ObjectPhoto = new FileViewModel
                    {
                        Id = claim.Photo.Id,
                        Content = claim.Photo.Content,
                        ContentType = claim.Photo.ContentType,
                        Name = claim.Photo.Name
                    };
                }

                if (mainDislike.Photos != null)
                {
                    foreach (var photo in mainDislike.Photos.ToList())
                    {
                        vm.Photos.Add(new FileViewModel
                        {
                            Id = photo.Id,
                            Content = photo.Content,
                            ContentType = photo.ContentType,
                            Name = photo.Name
                        });
                    }
                }

                foreach (var videoReference in mainDislike.VideoReferences.ToList())
                {
                    vm.VideoReferences.Add(videoReference.Reference);
                }

                res.Add(vm);
            }

            return res;
        }


        public static List<ClaimModel> ClaimsToVm(List<Claim> claims)
        {
            var res = new List<ClaimModel>();
            int i = 1;
            foreach (var claim in claims)
            {

                var mainDislike = claim.Dislikes.FirstOrDefault(d => d.MainDislike);

                if (mainDislike == null)
                    continue;

                var vm = new ClaimModel
                {
                    Id = claim.Id,
                    Name = claim.Name,
                    ObjectType = claim.ObjectType,
                    CreationDate = claim.CreationDate,
                    Region = string.Format("{0} {1}", claim.City.State.Country.Info, claim.City.Info)
                };

                if (claim.Photo != null)
                {
                    vm.ObjectPhoto = new FileViewModel
                    {
                        Id = claim.Photo.Id,
                        Content = claim.Photo.Content,
                        ContentType = claim.Photo.ContentType,
                        Name = claim.Photo.Name
                    };
                }

                vm.Place = i  + " место";
                i++;
             
                res.Add(vm);
            }

            return res;
        }

        public static DislikeModel DislikeToVm(Dislike dislike)
        {
            var vm = new DislikeModel
            {
                Id = dislike.Id,
                ClaimName = dislike.Claim.Name,
                Text = dislike.Text,
                DislikeType = dislike.DislikeType,
                CreationDate = dislike.CreationDate,
                ClaimPhotoId = dislike.Claim.PhotoId,
                Author = new UserVM
                {
                    Id = dislike.CreatorProfile.Id,
                    Login = dislike.CreatorProfile.Login
                },
                Category = dislike.Category,
                ClaimId = dislike.ClaimId
            };
            if (dislike.Photos != null)
            {
                vm.PhotosCount = dislike.Photos.Count;
                foreach (var photo in dislike.Photos.ToList())
                {
                    vm.Photos.Add(new FileViewModel
                    {
                        Id = photo.Id,
                        Content = photo.Content,
                        ContentType = photo.ContentType,
                        Name = photo.Name
                    });
                }
            }

            foreach (var videoReference in dislike.VideoReferences.ToList())
            {
                vm.VideoRefCount = dislike.VideoReferences.Count;
                vm.VideoReferences.Add(videoReference.Reference);
            }
            if (dislike.Docs != null)
            {
                vm.DocsCount = dislike.Docs.Count;
            }
            return vm;
        }

    }
}