﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using AntiRating.Domain.Entities;
using AntiRating.Models;

namespace AntiRating.Helpers
{
    public static class HtmlRequestHelper
    {
        public static string Controller(this HtmlHelper htmlHelper)
        {
            return Controller();
        }

        public static string Action(this HtmlHelper htmlHelper)
        {
            return Action();
        }

        internal static string Controller()
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("controller"))
                return (string)routeValues["controller"];

            return string.Empty;
        }

        internal static string Action()
        {
            var routeValues = HttpContext.Current.Request.RequestContext.RouteData.Values;

            if (routeValues.ContainsKey("action"))
                return (string)routeValues["action"];

            return string.Empty;
        }

        public static object ActionCheck(this HtmlHelper html, string actionName, string controllerName, string title)
        {
            var action = Action();
            var controller = Controller();
            var url = new UrlHelper(HttpContext.Current.Request.RequestContext);

            if (action.Equals(actionName, StringComparison.InvariantCultureIgnoreCase)
                && controller.Equals(controllerName, StringComparison.InvariantCultureIgnoreCase))
            {
                return title;
                //return MvcHtmlString.Create(title);
            }

            var a = new TagBuilder("a");
            a.MergeAttribute("href", url.Action(actionName, controllerName));
            a.InnerHtml = title;

            return MvcHtmlString.Create(a.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString ShowImage(this HtmlHelper html, Photo file)
        {
            return ShowImage(html, file.Id);
        }
        public static MvcHtmlString ShowImage(this HtmlHelper html, int fileId)
        {            
            var urlHelper = new UrlHelper(html.ViewContext.RequestContext);
            var url = urlHelper.Action("GetPhoto", "File", new {id = fileId});

            var tag = new TagBuilder("img");
            //tag.Attributes.Add("height", "100");
            tag.Attributes.Add("src", url);
            
            return new MvcHtmlString(tag.ToString());
        }

    }
}