﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Xml.Serialization;
using AntiRating.Attributes;

namespace AntiRating.Helpers
{
    public static class EnumExtensions
    {

        public static List<KeyValuePair<T, string>> EnumToKeyValuePairs<T>()
        {
            var pairs = new List<KeyValuePair<T, string>>();
            var values = Enum.GetValues(typeof(T));
            foreach (var value in values)
            {
                if (HasAttribute(value, typeof(NotDrawHtmlAttribute)))
                    continue;
                pairs.Add(new KeyValuePair<T, string>((T)value, GetEnumDescription(value)));
            }

            return pairs;
        }

        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            return (attributes.Length > 0) ? attributes[0].Description : value.ToString();
        }
        public static bool HasAttribute<TEnum>(TEnum value, Type attributeType)
        {
            var fi = value.GetType().GetField(value.ToString());

            var attributes = fi.GetCustomAttributes(attributeType, false);

            return (attributes.Length > 0);
        }

        public static string Description(this Enum value)
        {
            var enumType = value.GetType();
            var field = enumType.GetField(value.ToString());
            var attributes = field.GetCustomAttributes(typeof(DescriptionAttribute),
                false);
            return attributes.Length == 0
                ? value.ToString()
                : ((DescriptionAttribute)attributes[0]).Description;
        }

        /// <summary>
        /// Возвращает элемент Enum по значению атрибута XmlEnum
        /// </summary>
        /// <typeparam name="T">Тип перечисления</typeparam>
        /// <param name="attributeValue">Значение атрибута XmlEnum</param>
        /// <returns></returns>
        public static T GetEnumValue<T>(string attributeValue)
        {
            T res;
            if (attributeValue.TryConvertXmlToEnum(out res))
                return res;

            throw new ArgumentException(
                string.Format("Не найден элемент перечисления типа '{0}' со значением атртибута XmlEnum '{1}'.", typeof(T),
                    attributeValue));
        }

        public static bool TryConvertXmlToEnum<T>(this string value, out T res)
        {
            Enum en;
            var result = TryConvertXmlToEnum(value, typeof(T), out en);
            if (result)
                res = (T)(object)en;
            else
                res = default(T);
            return result;
        }

        public static bool TryConvertXmlToEnum(this string value, Type type, out Enum res)
        {
            return value.TryConvertXmlToEnum(false, type, out res);
        }

        public static bool TryConvertXmlToEnum(this string value, bool ignoreCase, Type type, out Enum res)
        {
            var map = CacheEnumValues(type);
            if (ignoreCase)
            {
                map = XmlEnumAttributeToEnumIgnoreCase[type];
                value = value.ToUpper();
            }
            object val;

            var result = map.TryGetValue(value ?? string.Empty, out val);
            res = (Enum)val;
            return result;
        }

        private static readonly Dictionary<Type, Dictionary<string, object>> XmlEnumAttributeToEnum = new Dictionary<Type, Dictionary<string, object>>();
        private static readonly Dictionary<Type, Dictionary<string, object>> XmlEnumAttributeToEnumIgnoreCase = new Dictionary<Type, Dictionary<string, object>>();


        private static Dictionary<string, object> CacheEnumValues(Type type)
        {
            Dictionary<string, object> map;
            if (!XmlEnumAttributeToEnum.TryGetValue(type, out map))
            {
                if (type.BaseType != typeof(Enum))
                {
                    throw new InvalidCastException();
                }

                map = new Dictionary<string, object>();
                var ignoreCaseMap = new Dictionary<string, object>();

                foreach (var member in type.GetFields())
                {
                    var custAttr = member.GetCustomAttributes(typeof(XmlEnumAttribute), false);
                    if (!custAttr.Any())
                        continue;
                    map[((XmlEnumAttribute)custAttr[0]).Name] = Enum.Parse(type, member.Name);
                    ignoreCaseMap[((XmlEnumAttribute)custAttr[0]).Name.ToUpper()] = Enum.Parse(type, member.Name);
                }
                XmlEnumAttributeToEnum[type] = map;
                XmlEnumAttributeToEnumIgnoreCase[type] = ignoreCaseMap;
            }
            return map;
        }

        public static string GetDisplayName(this Enum value)
        {
            var property = typeof(EnumExtensions).GetMethod("GetDisplayNameSlim");
            var generic = property.MakeGenericMethod(value.GetType());
            return (string)generic.Invoke(null, new object[] { value });
        }

    }
}