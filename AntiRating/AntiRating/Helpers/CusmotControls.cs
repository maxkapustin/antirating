﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using AntiRating.Attributes;

namespace AntiRating.Helpers
{
    public static class CusmotControls
    {

        private static Type GetNonNullableModelType(ModelMetadata modelMetadata)
        {
            Type realModelType = modelMetadata.ModelType;

            Type underlyingType = Nullable.GetUnderlyingType(realModelType);
            if (underlyingType != null)
            {
                realModelType = underlyingType;
            }
            return realModelType;
        }

        private static readonly SelectListItem SingleEmptyItem = new SelectListItem { Text = "", Value = "" };

        public static MvcHtmlString FromEnumDropDownListFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, object htmlAttributes = null, string optionLabel = null)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumType = GetNonNullableModelType(metadata);
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();

            var items = new List<SelectListItem>();
            foreach (var value in values)
            {

                if (EnumExtensions.HasAttribute(value, typeof(NotDrawHtmlAttribute)))
                    continue;

                var selectListItem = new SelectListItem
                {
                    Text = EnumExtensions.GetEnumDescription(value),
                    Value = value.ToString(),
                    Selected = value.Equals(metadata.Model)
                };
                items.Add(selectListItem);
            }

            if (metadata.IsNullableValueType)
                items.Insert(0, SingleEmptyItem);

            return htmlHelper.DropDownListFor(expression, items, optionLabel, htmlAttributes);
        }

        public static MvcHtmlString FromEnumDropDownList(this HtmlHelper htmlHelper, Type enumType, string name)
        {           
            var values = Enum.GetValues(enumType);

            var items = new List<SelectListItem>();
            foreach (var value in values)
            {

                if (EnumExtensions.HasAttribute(value, typeof(NotDrawHtmlAttribute)))
                    continue;

                var selectListItem = new SelectListItem
                {
                    Text = EnumExtensions.GetEnumDescription(value),
                    Value = value.ToString(),                    
                };
                items.Add(selectListItem);
            }

            items.Insert(0, SingleEmptyItem);

            return htmlHelper.DropDownList(name, items);
        }

    }
}