﻿UIHelper = {
    ShowBlock: function (block) {
        block.removeClass('hide');        
    },
    HideBlock: function (block) {        
        block.addClass('hide');
    }
}

CommonValidator = new function () {
    this.EmailIsValid = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
};

CommonHelper = new function () {
    this.executeFunctionByName = function (functionName, context /*, args */) {
        var args = [].slice.call(arguments).splice(2);
        var namespaces = functionName.split(".");
        var func = namespaces.pop();
        for(var i = 0; i < namespaces.length; i++) {
            context = context[namespaces[i]];
        }
        return context[func].apply(context, args);
    };

    this.addParamToUrl = function(url, param, value) {
        var a = document.createElement('a'), regex = /(?:\?|&amp;|&)+([^=]+)(?:=([^&]*))*/gi;
        var params = {}, match, str = [];
        a.href = url;
        while (match = regex.exec(a.search))
            if (encodeURIComponent(param) != match[1])
                str.push(match[1] + (match[2] ? "=" + match[2] : ""));
        str.push(encodeURIComponent(param) + (value ? "=" + encodeURIComponent(value) : ""));
        a.search = str.join("&");
        return a.href;
    };
    this.GetFormOfject = function (formId) {
        var formData = {};
        var arrayParams = $('#' + formId).serializeArray();
        for (var i = 0; i < arrayParams.length; i++) {
            formData[arrayParams[i].name] = arrayParams[i].value;
        }
        return formData;
    };
    this.alert = function (msg) {
        bootbox.alert(msg);
    };
    
    this.attention = function (title, msg) {
        $.gritter.add({
            title: title,
            text: msg
        });
        return false;
    };
    
    this.warn = function (msg, title) {
        bootbox.dialog({
            title: title || "Внимание",
            message: msg,
            closeButton: true,
            animate: true,
            className: "warning-modal",
            buttons: {
                success: {
                    label: "OK"
                }
            }
        });
    };
    this.Exception = function (model) {
        model.Title = model.Title == undefined ? 'Ошибка' : model.Title;
        model.Message = model.Message == undefined ? 'Неопознанная ошибка' : model.Message;
        model.ErrorDetails = model.ErrorDetails == undefined ? '' : model.ErrorDetails;

        bootbox.dialog({
            message: model.Message,
            title: model.Title,
            closeButton: true,
            animate: true,
            className: "error-modal",
            buttons: {
                success: {
                    label: "OK"
                }
            }
        });

    };
};