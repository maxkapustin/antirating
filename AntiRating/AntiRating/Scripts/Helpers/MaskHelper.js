﻿MaskHelper = new function () {
    var self = this;
    self.ShowBlockUi = function () {
        $.blockUI({
            message: '<div id="spinner" class="snipper-loading-mask">Идет обработка данных...</div>',
            css: {
                border: 'none',
                color: '#fff'
            },
            overlayCSS: {
                backgroundColor: '#002',
                opacity: 0.6,
                cursor: 'wait'
            },
            baseZ: 2000
        });
    };

    self.HideBlockUI = function () {
        $.unblockUI();
    };

    self.showMask = function (label, e, delay) {
        $(e).each(function () {
            if (delay !== undefined && delay > 0) {
                var element = $(this);
                element.data("_mask_timeout", setTimeout(function () { self.maskElement(element, label) }, delay));
            } else {
                self.maskElement($(this), label);
            }
        });
    };

    self.unmask = function (e) {
        $(e).each(function () {
            self.unmaskElement($(this));
        });
    };

    self.isMasked = function (e) {
        return e.hasClass("masked");
    };

    self.maskElement = function (element, label) {

        if (element.data("_mask_timeout") !== undefined) {
            clearTimeout(element.data("_mask_timeout"));
            element.removeData("_mask_timeout");
        }

        if (self.isMasked(element)) {
            self.unmaskElement(element);
        }

        if (element.css("position") == "static") {
            element.addClass("masked-relative");
        }

        element.addClass("masked");

        var maskDiv = $('<div class="loadmask"></div>');

        //auto height fix for IE
        if (navigator.userAgent.toLowerCase().indexOf("msie") > -1) {
            maskDiv.height(element.height() + parseInt(element.css("padding-top")) + parseInt(element.css("padding-bottom")));
            maskDiv.width(element.width() + parseInt(element.css("padding-left")) + parseInt(element.css("padding-right")));
        }

        if (navigator.userAgent.toLowerCase().indexOf("msie 6") > -1) {
            element.find("select").addClass("masked-hidden");
        }

        element.append(maskDiv);

        if (label !== undefined) {
            var maskMsgDiv = $('<div class="loadmask-msg" style="display:none;"></div>');
            maskMsgDiv.append('<div>' + label + '</div>');
            element.append(maskMsgDiv);

            //calculate center position
            maskMsgDiv.css("top", Math.round(element.height() / 2 - (maskMsgDiv.height() - parseInt(maskMsgDiv.css("padding-top")) - parseInt(maskMsgDiv.css("padding-bottom"))) / 2) + "px");
            maskMsgDiv.css("left", Math.round(element.width() / 2 - (maskMsgDiv.width() - parseInt(maskMsgDiv.css("padding-left")) - parseInt(maskMsgDiv.css("padding-right"))) / 2) + "px");

            maskMsgDiv.show();
        }

    };

    self.unmaskElement = function (element) {
        if (element.data("_mask_timeout") !== undefined) {
            clearTimeout(element.data("_mask_timeout"));
            element.removeData("_mask_timeout");
        }

        element.find(".loadmask-msg,.loadmask").remove();
        element.removeClass("masked");
        element.removeClass("masked-relative");
        element.find("select").removeClass("masked-hidden");
    };

};