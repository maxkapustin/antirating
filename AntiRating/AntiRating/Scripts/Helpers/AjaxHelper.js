﻿AjaxHelper = {
    ajaxRequest: function (url, data, success, error, mimeType, showMask, showErrorMessage) {
        if (showMask) {            
            MaskHelper.ShowBlockUi();
        }
        return jQuery.ajax({
            dataType: mimeType,
            //timeout: 120000,
            url: url,
            type: 'POST',
            cache: false,
            data: data,
            success: function (jsondata) {
                if (showMask) {                    
                    MaskHelper.HideBlockUI();

                }
                //console.warn(jsondata);
                if (mimeType == 'json') {
                    if (jsondata.success === false) {
                        if (showErrorMessage)
                            CommonHelper.Exception(jsondata.data);
                        if (error)
                            error(jsondata.data);
                    } else {
                        if (success)
                            success(jsondata.data);
                    }
                } else {
                    var len = jsondata.length;
                    var isError = len > 0 && jsondata.charAt(0) == '{' && jsondata.charAt(len - 1) == '}';
                    if (isError) {
                        try {
                            var json = JSON.parse(jsondata);
                            if (showErrorMessage)
                                CommonHelper.Exception(json);
                            if (error)
                                error(json);
                        } catch (e) {
                            isError = false;
                        }
                    }
                    if (!isError) {
                        if (success)
                            success(jsondata);
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                if (showMask) {                    
                    MaskHelper.HideBlockUI();
                }
                if (error)
                    error({
                        success: false,
                        Title: 'Ошибка',
                        Message: xhr.status + ': ' + xhr.statusText,
                        ErrorDetails: xhr.responseText
                    });
            }
        });
    },

    loadTreeViewModelItem: function (url, dataParams, success, error) {
        return this.loadJsonData(url, dataParams, success, error);
    },

    loadJsonData: function (url, dataParams, success, error) {
        return this.ajaxRequest(url, dataParams, success, error, 'json');
    },

    loadJsonData2: function (url, dataParams, success, error) {
        return this.ajaxRequest(url, dataParams, success, error, 'json');
    },

    loadHtmlData: function (url, dataParams, success, error) {
        return this.ajaxRequest(url, dataParams, success, error, 'html');
    },

    loadHtml: function (url, dataParams, selector, success, error, showMask) {
        return this.ajaxRequest(
			url,
			dataParams,
			function (resp) {
			    jQuery(selector).html(resp);
			    if (success) success(resp);
			},
			function (resp) {
			    jQuery(selector).html('<div class="alert alert-danger" style="white-space: pre-wrap;" role="alert"></div>');
			    jQuery(selector).find('.alert').text(resp.Message);
			    if (error) error(resp);
			},
			'html', showMask);
    },

    ajaxRequestJsonData: function (url, dataParams, success, error, showMask) {
        return this.ajaxRequest(url, dataParams, success, error, 'json', showMask);
    },

    ajaxRequestXmlData: function (url, dataParams, success, error) {
        return this.ajaxRequest(url, dataParams, success, error, 'xml');
    }
}