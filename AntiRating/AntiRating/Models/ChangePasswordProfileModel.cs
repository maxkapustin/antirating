﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AntiRating.Models
{
    public class ChangePasswordProfileModel
    {
        [Required(ErrorMessage = "{0} обязательное поле для заполнения")]
        [StringLength(100, ErrorMessage = "{0} должен быть не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Старый пароль")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "{0} обязательное поле для заполнения")]
        [StringLength(100, ErrorMessage = "{0} должен быть не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Новый пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Потвердите новый пароль")]
        [Compare("Password", ErrorMessage = "Пароль и потверждение пароля разные.")]
        public string ConfirmPassword { get; set; }

        public string ReturnUrl { get; set; }
    }
}