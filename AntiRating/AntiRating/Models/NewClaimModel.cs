﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using AntiRating.Domain;

namespace AntiRating.Models
{
    public class NewClaimModel: ClaimModel
    {

        public NewClaimModel()
        {
            Photos = new List<FileViewModel>();
            VideoReferences = new List<string>();
            Docs= new List<FileViewModel>();
        }

        public int? MainDislikeId { set; get; }

        public List<FileViewModel> Photos { set; get; }

        public List<FileViewModel> Docs { get; set; }

        [Display(Name = "Ссылки на  видеоматериалы по жалобе")]
        public List<string> VideoReferences { get; set; }

        [Required(ErrorMessage = "Данное поле обязательно")]
        [Display(Name = "Суть жалобы")]
        public string Text { get; set; }

        [Required(ErrorMessage = "Населенный пункт обязателен")]        
        public Guid? CityId { set; get; }


        [Display(Name = "В чем притензия?")]
        public DislikeCategory DislikeCategory { get; set; }
    }
}