﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Web;

namespace AntiRating.Models
{
    public class FileViewModel
    {

        public HttpPostedFileBase FileBase { get; set; }

        public int? Id { set; get; }        
        public byte[] Content { get; set; }
       
        public string Name { get; set; }
    
        public string ContentType { get; set; }
    }
}