﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AntiRating.Domain;

namespace AntiRating.Models
{
    public class DislikeModel
    {
        public DislikeModel()
        {
            Photos = new List<FileViewModel>();
            VideoReferences = new List<string>();
        }
        public int? Id { get; set; }

        public int ClaimId { get; set; }
        public string ClaimName { get; set; }
        public int? ClaimPhotoId { get; set; }

        public string Text { get; set; }

        public DislikeType DislikeType { get; set; }

        public List<string> VideoReferences { get; set; }

        public List<FileViewModel> Photos { get; set; }

        public List<FileViewModel> Docs { get; set; }

        public DateTime CreationDate { get; set; }

        public DislikeCategory Category { get; set; }

        public UserVM Author { get; set; }

        public int? DislikeCounts { set; get; }

        public int? VideoRefCount { set; get; }
        public int? PhotosCount { set; get; }
        public int DocsCount { get; set; }
    }
}