﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AntiRating.Models
{
    public class ProfileModel
    {
       
        [StringLength(50)]
        [Display(Name = "Логин")]
        public string Login { get; set; }


        [Display(Name = "e-mail")]
        [StringLength(50)]
        [EmailAddress(ErrorMessage = "Не верный email")]
        public string Email { get; set; }

        [Display(Name = "Фото объекта")]
        public FileViewModel Photo { get; set; }

        [Display(Name = "Имя")]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [StringLength(50)]
        public string SecondName { get; set; }

        [Display(Name = "почтовый ящик показывать")]
        public bool EmailVisible { get; set; }

        public int BonusDislikeCount { get; set; }
    }
}