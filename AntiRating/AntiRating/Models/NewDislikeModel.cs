﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AntiRating.Domain;

namespace AntiRating.Models
{
    public class NewDislikeModel
    {
        public ClaimModel Claim { get; set; }

        public int ClaimId { get; set; }

        public int ProfileId { get; set; }

        

        [Display(Name = "Фотоматериалы по жалобе")]
        public HttpPostedFileBase[] Photos { get; set; }

        [Display(Name = "Документы")]
        public HttpPostedFileBase[] Docs { get; set; }

        [Display(Name = "Ссылки на  видеоматериалы по жалобе")]
        public string[] VideoReferences { get; set; }
         
        [Required(ErrorMessage = "Данное поле обязательно")]
        [Display(Name = "Суть жалобы")]
        public string Text { get; set; }

        [Display(Name = "Тип поста")]
        public DislikeType DislikeType { get; set; }

        public DateTime? CreationDate { get; set; }

        [Display(Name = "Категория")]
        public DislikeCategory Category { get; set; }
    }
}