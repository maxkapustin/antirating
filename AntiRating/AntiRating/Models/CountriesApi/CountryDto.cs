﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AntiRating.Models.CountriesApi
{
    public class VkResponseDto
    {
        public VkResponse Response { set; get; }
    }

    public class VkResponse
    {
        public int Count { set; get; }
        public List<VkItem> Items { set; get; } 
    }

    public class VkItem
    {
        public int Id { set; get; }
        public string Title { set; get; }
    }
}