﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using AntiRating.Domain;


namespace AntiRating.Models
{
    public class ClaimModel
    {

        public ClaimModel()
        {            
            ObjectPhoto = new FileViewModel();
            Dislikes = new List<DislikeModel>();
            VideoReferenceModel = new List<VideoReferenceModel>();
        }

        public int? Id { set; get; }
        public int ProfileId { set; get; }
        public DateTime? CreationDate { set; get; }

        [Required(ErrorMessage = "Данное поле обязательно")]
        [Display(Name = "Имя нехорошего человека")]
        public string Name { get; set; }
        public string Region { get; set; }

        [Display(Name = "На кого жалуетесь?")]
        public ClaimObjectType ObjectType { get; set; }
        
        public List<VideoReferenceModel> VideoReferenceModel { get; set; }

        [Display(Name = "Фото объекта")]
        public FileViewModel ObjectPhoto { get; set; }        

        public List<DislikeModel> Dislikes;

        public string Place { get; set; }

        public int CurrentCategoryDislikeCount { get; set; }

        public int ViewCount { set; get; }

    }

    public class VideoReferenceModel
    {
        public int? Id { set; get; }
        public string Url { set; get; }
    }
}