﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AntiRating.Domain.Entities;

namespace AntiRating.Models
{
    public class MainRating
    {
        public List<ClaimModel> FraudsterRatings { set; get; }

        public List<ClaimModel> ThiefRatings { set; get; }

        public List<ClaimModel> BribetakerRatings { set; get; }

        public List<ClaimModel> MarriageSwindlerRatings { set; get; }

        public List<ClaimModel> AlimonyRatings { set; get; }
        public List<ClaimModel> DistasteRatings { set; get; }
        public List<ClaimModel> DeceiversRatings { set; get; }
        public List<ClaimModel> InfidelityRatings { set; get; }
        public List<ClaimModel> SlanderRatings { set; get; }
        public List<ClaimModel> UnscrupulousEmployersRatings { set; get; }
        public List<ClaimModel> UnscrupulousEmployeesRatings { set; get; }

        public List<DislikeModel> UserDislikes { set; get; }

        public List<DislikeModel> MyTop4Dislikes { set; get; }
    }
}