﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AntiRating.Domain;

namespace AntiRating.Models
{
    public class SearchModel
    {

        public Guid? FilterCountry { set; get; }
        public Guid? FilterState { set; get; }
        public Guid? FilterCity { set; get; }

        public string FilterName { set; get; }
        public DislikeCategory? DislikeCategory { set; get; }

    }
}