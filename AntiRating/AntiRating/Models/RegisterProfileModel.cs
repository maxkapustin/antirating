﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AntiRating.Models
{
    public class RegisterProfileModel
    {
        [Required(ErrorMessage = "{0} обязательное поле для заполнения")]
        [StringLength(50)]
        [Display(Name = "Логин")]
        public string Login { get; set; }


        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Не верный email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "{0} обязательное поле для заполнения")]
        [StringLength(100, ErrorMessage = "{0} должен быть не менее {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Потвердите пароль")]
        [Compare("Password", ErrorMessage = "Пароль и потверждение пароля разные.")]
        public string ConfirmPassword { get; set; }

        public string ReturnUrl { get; set; }
    }
}